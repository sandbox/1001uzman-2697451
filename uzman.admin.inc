<?php

/**
 * Implementation of hook_admin_settings() for configuring the module
 *
 * @param array $form_state
 *   structure associative drupal form array
 * @return array
 */
function uzman_admin_settings_form()
{
    $form['uzman'] = array(
        '#type' => 'vertical_tabs',
    );

    // General Settings
    $form['account'] = array(
        '#type' => 'fieldset',
        '#title' => t('Genel ayarlar'),
        '#collapsible' => FALSE,
        '#group' => 'uzman',
    );


    $form['account']['bilgi']= array(
        '#markup' => '<p>' . t('Bu üç parametreyi 1001uzman.com\'daki uzmanlık hesabınızdan alabilirsiniz. Eğer bir hesabınız yoksa !url bir profil oluşturabilirsiniz.',  array('!url' => l('buraya tıklayarak', 'https://1001uzman.com/hosgeldin', array('attibutes' => array('target' => '_blank'))))) . '</p>' .
            '<p>' . t('!url sayfasına gidin, giriş yapın, sayfanın en altındaki parametreleri buraya yapıştırın.', array('!url' => l('1001uzman.com widget oluşturma', 'https://1001uzman.com/uzman/widget', array('attibutes' => array('target' => '_blank'))))) . '</p>' ,
    );

    $form['account']['uzman_account'] = array(
        '#type' => 'textfield',
        '#title' => t('Widget kodu'),
        '#default_value' => variable_get('uzman_account', ''),
        '#size' => 40,
        '#maxlength' => 40,
        '#required' => TRUE,
        '#description' => t("Bu kod size özel profili ayırt ettiren ve eklentinin renk ve diğer ayarlarını tesbit ettiren özel bir koddur."),


    );


    $form['account']['uzman_isyeri_adi'] = array(
        '#type' => 'textfield',
        '#title' => t('Firma adı'),
        '#default_value' => variable_get('uzman_isyeri_adi', ''),
        '#size' => 40,
        '#maxlength' => 40,
        '#required' => TRUE,
    );

    $form['account']['uzman_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Profil urlsi'),
        '#default_value' => variable_get('uzman_url', ''),
        '#size' => 40,
        '#maxlength' => 40,
        '#required' => TRUE,
    );


    // Role specific visibility configurations.
    $form['role_vis_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Rollere özel ayarlar'),
        '#collapsible' => TRUE,
        '#group' => 'uzman',
    );

    $roles = user_roles();
    $role_options = array();
    foreach ($roles as $rid => $name) {
        $role_options[$rid] = $name;
    }
    $form['role_vis_settings']['uzman_roles'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Seçtiğim rollerde widget görünmesin'),
        '#default_value' => variable_get('uzman_roles', array()),
        '#options' => $role_options,
        '#description' => t('Widget sadece seçilen rollerde görünmeyecektir. Eğer hiç birisi seçili değilse hepsinde widget görünecektir.'),
    );
    // END Role specific visibility configurations.

    // Page specific visibility configurations.
    $form['page_vis_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Sayfalara özel görünüm ayarları'),
        '#collapsible' => TRUE,
        '#group' => 'uzman',
    );

    $access = user_access('Uzman widget gorunum PHP ile');
    $visibility = variable_get('uzman_visibility', 0);
    $pages = variable_get('uzman_pages', '');

    if ($visibility == 2 && !$access) {
        $form['page_vis_settings'] = array();
        $form['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
        $form['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
    } else {
        $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
        $description = t("Drupal yolunu kullanarak her satıra tek bir sayfa girin.  '*' karakteri jokerdir.Örnek: ağ günlüğü sayfası için %blog; tüm kişisel ağ günlükleri için %blog-wildcard. Anasayfa için %front kullanınız.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
        if ($access) {
            $options[] = t('PHP kodu kullanarak görünecek sayfalarda <code>TRUE</code> dönderin (PHP-modu, yalnızca tecrübeliler).');
            $description .= ' ' . t('Eğer PHP-modunu seçerseniz, PHP kodunuz %php tagları arasına yazın. Drupal siteniz kırılırsa yanlış bir kod kullanmışsınız demektir.', array('%php' => '<?php ?>'));
        }
        $form['page_vis_settings']['uzman_visibility'] = array(
            '#type' => 'radios',
            '#title' => t('Sayfalara özel görünüm ayarları'),
            '#options' => $options,
            '#default_value' => $visibility,
        );
        $form['page_vis_settings']['uzman_pages'] = array(
            '#type' => 'textarea',
            '#title' => t('Pages'),
            '#default_value' => $pages,
            '#description' => $description,
            '#wysiwyg' => FALSE,
        );
    }
    // END Page specific visibility configurations.

    return system_settings_form($form);
}

/**
 * Implementation of hook_admin_settings_form_validate().
 *
 * @param array $form
 *   structured associative drupal form array.
 * @param array $form_state
 */
function uzman_admin_settings_form_validate($form, &$form_state)
{
    if (empty($form_state['values']['uzman_account'])) {
        form_set_error('uzman_account', t('Bir uzman koduna ihtiyaç var. Lütfen açıklamayı okuyun'));
    } else {


//        $kod = $form_state['values']['uzman_account'];
//
//
//        $content = file_get_contents("https://1001uzman.com/ajax/su_koda_gore_uzman_linkini_cek?kod=" . $kod);
//        $data = json_decode($content);
//
//        dsm($data);
//
    }
}
